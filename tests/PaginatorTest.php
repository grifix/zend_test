<?php

namespace App\Test;

use App\MyPaginator;
use PHPUnit\Framework\TestCase;
use Zend_Db;
use Zend_Db_Adapter_Pdo_Pgsql;
use Zend_Db_Table;
use Zend_Paginator_Adapter_DbTableSelect;

class PaginatorTest extends TestCase
{
    /** @var Zend_Db */
    private $db;

    public function setUp()
    {
        parent::setUp();
        $this->createDbConnection();
        $this->createTable();
        $this->seedData();
        $this->db->getProfiler()->setEnabled(true);
    }

    public function testItPaginates()
    {
        $paginator = $this->createPaginator()
            ->setItemCountPerPage(10)
            ->setCurrentPageNumber(1)
        ;
        $items = [];
        $this->db->getProfiler()->clear();
        foreach ($paginator as $item) {
            $items[] = $item;
        }
        self::assertEquals(10, count($items));
        self::assertEquals(51, $items[0]['id']);
        self::assertEquals(60, $items[9]['id']);
        self::assertEquals(1, $this->db->getProfiler()->getTotalNumQueries());
    }

    public function testItPaginatesInfinite()
    {
        $paginator = $this->createPaginator()
            ->setItemCountPerPage(10)
            ->setCurrentPageNumber(1)
            ->setInfiniteMode(true)
        ;
        $items = [];
        $this->db->getProfiler()->clear();
        foreach ($paginator as $item) {
            $items[] = $item;
        }
        self::assertEquals(50, count($items));
        self::assertEquals(51, $items[0]['id']);
        self::assertEquals(100, $items[49]['id']);
        self::assertEquals(5, $this->db->getProfiler()->getTotalNumQueries());
    }

    /**
     * @return MyPaginator
     */
    private function createPaginator()
    {
        $dbTable = new Zend_Db_Table(['db' => $this->db, 'name' => 'test.items']);
        $select = $dbTable->select()->where('id > ?', 50)->order('id asc');
        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
        return new MyPaginator($adapter);
    }

    private function createDbConnection()
    {
        $this->db = new Zend_Db_Adapter_Pdo_Pgsql(array(
            'host' => 'db',
            'username' => 'zend',
            'password' => 'zend',
            'dbname' => 'zend'
        ));
    }

    private function createTable(){
        $this->db->query('drop table if exists test.items')->execute();
        $sql = <<<SQL
create table if not exists test.items
(
	id serial not null
		constraint items_pk
			primary key,
	name char(255) not null
)
SQL;
        $this->db->query($sql)->execute();
    }

    private function seedData(){
        for ($i = 0; $i < 100; $i++) {
            $this->db->insert('test.items', ['name' => sprintf('item_%d', $i)]);
        }
    }
}
