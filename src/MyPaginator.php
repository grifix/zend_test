<?php

namespace App;

use Zend_Paginator;

class MyPaginator extends Zend_Paginator
{
    /** @var bool */
    private $infiniteMode = false;

    /**
     * @param bool $infiniteMode
     * @return $this
     */
    public function setInfiniteMode($infiniteMode)
    {
        $this->infiniteMode = $infiniteMode;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrentItems()
    {
        if ($this->infiniteMode) {
            while ($this->getCurrentPageNumber() != $this->_calculatePageCount()) {
                $items = parent::getCurrentItems();
                $this->setCurrentPageNumber($this->getCurrentPageNumber() + 1);
                foreach ($items as $item) {
                    yield $item;
                }
            }
        }
        foreach (parent::getCurrentItems() as $item) {
            yield $item;
        }
    }
}
